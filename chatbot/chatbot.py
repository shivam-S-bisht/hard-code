# curl -X POST -H "Content-Type:application/json" -d '{"response":"hi"}' http://127.0.0.1:5000/
import random
import re



pairs = [

    [   
        r'(.*)name is(.*)',
        ['hi there, its nice to meet you, btw Im Chatty', 'Did I forget to intoduce myself? Hi, Iam Chatty the chatbot']
    ],

    
  

    [
        r'(.*)(members|brl)(.*)(members|brl)(.*)',
        ['There are total 32 core members which complete the BRL :) In my knowledge, They all are very hardworking.']
    ],

    [
        r'(.*)your name',
        ['meet me.... I am Chatty the chat bot', 'u can call me Chatty']
    ],

    [
        r'((.*)help (me|my))|(.*)you do(.*)',
        ['i can help you, with Blockchain related querries']
    ],

    [
        r'(.*)(created|made|invented) you',
        ['top secret', 'I am a lovely creation of BRL developers']
    ],

    [
        r'(.*)(about|is|about the) blockchain(.*)',
        ['The blockchain is a decentralized distributed database of immutable records. The technology was discovered with the invention of Bitcoins(the first cryptocurrency). It’s a trusted approach and there are a lot of companies in the present scenario which are using it. As everything is secure, and because it’s an open source approach, it can easily be trusted in the long run']
    ],
    
    [   
        r'(.*)(features|properties) of blockchain(.*)',
        ['Decentralized Systems, Distributed ledger, Safer & Secure Ecosystem and Minting are the key features of Blockchain.']
    ],
    
    [
        r'(.*|blockchain) encryption(.*|blockchain)',
        ['Data security always matters. Encryption is basically an approach that helps organizations to keep their data secure. In Blockchain, this approach is useful because it simply adds more to the overall security and authenticity of blocks and helps to keep them secure.']
    ],
    
    [
        r'(.*)blocks(.*)blockchain(.*)',
        ['Blockchain consists of a list of records. Such records are stored in blocks. These blocks are in turn linked with other blocks and hence constitute a chain called Blockchain.']
    ],

    [   
        r'(.*)ledger(.*)blockchain(.*)',
        ['Centralised, De-centrlised and Distributed ledger are the most common types of ledger in Blockchain.']
    ],
    
    [
        r'(.*)(main|major) (component|components) of blockchain(.*)',
        ['Shared ledger, node application, virtual machines and concensus algorithms are the major components of Blockchain ecosystem.']
    ],
    
    [
        r'(.*)(benefits|advantages|importance)(.*)blockchain(.*)',
        ['Settlement in real time, Cost saving, Security and reliance, Immutability and user Pseudonymity are the benefits of blockchain']
    ],
    
    [   
        r'(.*)applications(.*)blockchain(.*)',
        ['The applications of blockchain is beyond crytocurrency. Like finance, music, healthcare, passport and personall identification.', 'asset management, payments, insaurance clims, hard money lending, cross border payments,.....i m tired naming them :|']
    ],

    [
        r'(.*)thank(.*)',
        ['Always eager to help :)', 'Happy to see you happy']
    ],
    

    [
        r'(.*)(quit|bye)',
        ['byee, see you soon :)', 'it was nice talking to you']
    ],

    [
        r'(.*)',
        ['really..', 'that was good to hear', 'sorry what?']
    ],

    [   r'(.*)(hi|hello|hola|helliew|hey)(.*)',
        ['hello', 'hi there', 'holla']
    ],

    

]




def bot(inp):

    
    if not inp == 'quit':
        for examples in pairs:
            sequence, responses = examples
            if re.search(sequence, inp):
                yield random.choice(responses)
                break
    else:
        yield random.choice(pairs[-1][1])
        exit()





import json
from flask import Flask, render_template, request, jsonify, redirect

app = Flask(__name__)

@app.route('/', methods = ['POST', 'GET'])
def home():
    if request.method == 'POST':
        some_json = request.get_data(as_text = True)
        
    
        for response in bot(some_json):
            return jsonify({'response' : response})

    else:
        return None
